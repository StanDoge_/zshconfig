# Path to your oh-my-zsh installation. ----------------------
export ZSH="/home/standoge/.oh-my-zsh"
export GPG_TTY=$(tty)

# Typewritting as prompt -----------------------------------
fpath+=$HOME/.zsh/typewritten
# export TYPEWRITTEN_SYMBOL="🐸 ❯"
export TYPEWRITTEN_SYMBOL="🐸"
export TYPEWRITTEN_PROMPT_LAYOUT="singleline"
autoload -U promptinit; promptinit
prompt typewritten

# OMZ config  ------------------------------------------------
#ZSH_THEME="" #refined == duru #frontcube #sorin #kardan #zsh2000 #nicoulaj

# OMZ propm config    --------------------------------------------
#export ZSH_2000_DISABLE_RIGHT_PROMPT='false'
#export ZSH_2000_DISABLE_RVM='true'
#export ZSH_2000_DEFAULT_USER='YOUR_USER_NAME'

# zsh behavior configs ------------------------------------------------
 HYPHEN_INSENSITIVE="true"
 COMPLETION_WAITING_DOTS="true"

# zsh pluggins  ------------------------------------------------
plugins=(git colored-man-pages 
	zsh-autosuggestions 
	zsh-syntax-highlighting
	ubuntu
	vscode
	docker)

source $ZSH/oh-my-zsh.sh

# Vim use on terminal ------------------------------------------
bindkey -v

# User configuration ---------------------------------------------
 export LANG=en_US.UTF-8 #enviroment language

if [[ -n $SSH_CONNECTION ]]; then #editor for local and remote sessions
  export EDITOR='vim'
 else
  export EDITOR='vim'
fi

# Personal aliases ------------------------------------------------
alias l='exa -B --long --header --inode  --group-directories-first'
alias q="cd ~ ; exit"
alias f="explorer.exe ." 
alias bat="batcat"
alias hub="/mnt/c/Users/kevin/Downloads"
alias dev="cd ~/development ; l"
alias drive="/mnt/g/My\ Drive/Universidad/ ; l" 
alias fresh="cd ; source .zshrc"
alias hackear="cd ; nvim .zshrc"
alias gcache="git rm -r --cached . && git add ."
alias gcams="git commit -S -am $1"
alias vcache="rm -rf ~/.config/nvim && rm -rf ~/.local/share/nvim && rm -rf ~/.cache/nvim"
alias fs="du -sh"
alias py=python3
alias dpress='/home/standoge/development/Bash/dpress/dpress.sh'
alias tasks='sudo service cron start'

# Personal functions ------------------------------------------------
mk(){
  #make a dir and cd within
  mkdir $1 ; cd $1
}

# Miniconda3 setup ------------------------------------------------

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/standoge/dev_dependencies/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/standoge/dev_dependencies/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/standoge/dev_dependencies/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/standoge/dev_dependencies/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
# to enable autocomplete
zstyle ':completion:*:*:docker:*' option-stacking yes
zstyle ':completion:*:*:docker-*:*' option-stacking yes
